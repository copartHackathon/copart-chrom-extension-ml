package com.copart.Rprog;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;

import org.rosuda.REngine.REXPMismatchException;
import org.rosuda.REngine.Rserve.RConnection;
import org.rosuda.REngine.Rserve.RserveException;

public class RScript {
	static ArrayList<String> suggestedItems = new ArrayList();
	
	public static void main(String a[]) throws IOException {
        RConnection connection = null;

        try {
            /* Create a connection to Rserve instance running
             * on default port 6311
             */
            connection = new RConnection();

            String vector = "c(1,2,3,4)";
            connection.eval("meanVal=mean(" + vector + ")");
            connection.eval("source('/Users/prram/Downloads/Copart_Hackathon/Hackathon.R')");
            connection.eval("Suggestions()");
            double mean = connection.eval("meanVal").asDouble();
            
            System.out.println("The mean of given vector is=" + mean);
            parseOutput();
            
        } catch (RserveException e) {
            e.printStackTrace();
        } catch (REXPMismatchException e) {
            e.printStackTrace();
        }finally{
            connection.close();
        }
        
        
    }
	
	public static void parseOutput() throws IOException{
		
    	 String line;
		 String splitBy = ",";
	      BufferedReader br = new BufferedReader(new FileReader("/Users/prram/Downloads/Copart_Hackathon/End_output.csv"));
	      br.readLine();
	      while((line = br.readLine()) != null){
	           String[] b = line.split(splitBy);
	           if(b[6].equals("1")){
	        	   suggestedItems.add(b[2]+b[3]+b[4]);
	           }
	           
	      }
	      br.close();
	      System.out.println(suggestedItems);
    }

}
