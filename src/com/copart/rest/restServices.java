package com.copart.rest;

import javax.ws.rs.FormParam;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Response;

@Path("/")
public interface restServices {

	/*
	 * Register User into Getto
	 */
	@POST
	@Produces("application/json")
	@Path("/register")
	public Response userRegister(@FormParam("phoneNum") String phoneNum, @FormParam("name") String name,
			@FormParam("password") String password);

}