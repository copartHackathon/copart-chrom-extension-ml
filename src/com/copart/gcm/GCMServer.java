package com.copart.gcm;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import com.copart.gcm.Message;
import com.copart.gcm.MulticastResult;
import com.copart.gcm.Sender;

public class GCMServer {
	
	public static void main(String args[]) throws IOException{
		GCMServer t = new GCMServer();
		ArrayList<String> temp = new ArrayList<>();
		temp.add("APA91bGQLERAgFuZJzKNsR7Vd5ctXLlp_I_z8YKTfnJc2K9v1PfL67QbhQiF9NjMgqS1zXlRT3Z9hTgQY00XsWeT4r7z1VXl65cwLjapCJ9v21m7EO81DcD0Pbe3qffjsvMAvYfW19M65DijcCi1Ym8BgaUmf-k4bg");
		t.sendMessage("hello", temp);
	}

	public void sendMessage(String messageStr, ArrayList<String> androidTargets) throws IOException {
		String API_KEY = "AIzaSyANd0mxHl9TohwYZE0CVUxtQ-A1uRhctTw"; 
		String collpaseKey = "gcm_message"; 
		//String messageStr = "New user added to the lobby"; // actual message content
		//String messageId = "APA91bES_0lqJ1AbCqH2e36unZgh7C0Glaf8BC5HH_JnxHQ-mF8zfpGsrE10BQPmP3EMyoBNOpJkV6xh9fjVoV16fnHusOfenqOOYEfp_J-gZ-P6TykX56pEhpVWczJuWNqJpSLTbjSe"; // gcm
		Sender sender = new Sender(API_KEY);
		Message.Builder builder = new Message.Builder();
		builder.collapseKey(collpaseKey);
		builder.timeToLive(30);
		builder.delayWhileIdle(true);
		builder.addData("title", messageStr);
		Message message = builder.build();
		//List<String> androidTargets = new ArrayList<String>();
		// if multiple messages needs to be deliver then add more message ids to
		// this list
		//androidTargets.add(messageId);
		MulticastResult result = sender.send(message, androidTargets, 1);
		System.out.println("result = " + result);
		if (result.getResults() != null) {
			int canonicalRegId = result.getCanonicalIds();
			System.out.println("canonicalRegId = " + canonicalRegId);
			if (canonicalRegId != 0) {
			}
		} else {
			int error = result.getFailure();
			System.out.println("Broadcast failure: " + error);
		}
	}
}
